TARGET_BASE_NAME := $(shell cat BASENAME)

TARGET_PR = $(TARGET_BASE_NAME).pr
TARGET_HO = $(TARGET_BASE_NAME).ho
TARGET_HO2 = $(TARGET_BASE_NAME).ho2

TARGET_PRESENTATION = $(TARGET_BASE_NAME)-presentation
TARGET_HANDOUT = $(TARGET_BASE_NAME)-handout
TARGET_HANDOUT2 = $(TARGET_BASE_NAME)-handout2
TARGET_PAPER = $(TARGET_BASE_NAME)-paper
TARGET_BASE = $(TARGET_BASE_NAME)-base

TARGET = $(TARGET_BASE_NAME)

DIAIMAGES     = design-ctdb-three-nodes.dia ctdb-design-daemons.dia samba-layers.dia samba-release-stream.dia

DIAIMAGES_PNG = design-ctdb-three-nodes.png ctdb-design-daemons.png samba-layers.png samba-release-stream.png

DIAIMAGES_FIG = design-ctdb-three-nodes.fig ctdb-design-daemons.fig samba-layers.fig

DIAIMAGES_SVG = design-ctdb-three-nodes.svg ctdb-design-daemons.svg samba-layers.svg

#IMAGES = $(DIAIMAGES_PNG) \
#	 regedit.png \
#	 ctdb-status.png \
#	 ctdb-status-1.png \
#	 ctdb-ip.png \
#	 ctdb-ip-1.png \
#	 smbstatus.png

IMAGES = $(DIAIMAGES_PNG)

CMN_DEPS = Makefile $(IMAGES)

CMN_DEPS_WIKI = $(CMN_DEPS) document.part1.wiki document.part2.wiki document.part3.wiki content.wiki info.wiki

CMN_DEPS_TEX = $(CMD_DEPS) beamercolorthemeobnoxsamba.sty beamerouterthemeobnoxinfolines.sty beamerthemeObnoxSamba.sty

COMMON_DEPS = $(CMN_DEPS) content.tex base.tex

VIEWER = evince
SHOW = yes

.SUFFIXES: .tex .pdf .dia .png .fig .svg .wiki

.PHONY: all

all: pr

#all: $(TARGET_PRESENTATION) $(TARGET_HANDOUT) $(TARGET_HANDOUT2)


.PHONY: pr $(TARGET_PR)

pr: $(TARGET_PR)

$(TARGET_PR): $(TARGET_PR).pdf
	if [ "$(SHOW)" = "yes" ]; then $(VIEWER) $@.pdf ; fi &

$(TARGET_PR).pdf: $(CMN_DEPS) pr.pdf
	cp pr.pdf $@

pr.pdf: $(CMN_DEPS_TEX) pr.tex

pr.tex: $(CMN_DEPS_WIKI) pr.wiki

pr.wiki: $(CMN_DEPS_WIKI) pr.class.wiki
	cat document.part1.wiki pr.class.wiki document.part2.wiki info.wiki document.part3.wiki > $@


.PHONY: ho $(TARGET_HO)

ho: $(TARGET_HO)

$(TARGET_HO): $(TARGET_HO).pdf
	if [ "$(SHOW)" = "yes" ]; then $(VIEWER) $@.pdf ; fi &

$(TARGET_HO).pdf: $(CMN_DEPS) ho.pdf
	cp ho.pdf $@

ho.pdf: $(CMN_DEPS_TEX) ho.tex

ho.tex: $(CMN_DEPS_WIKI) ho.wiki

ho.wiki: $(CMN_DEPS_WIKI) ho.class.wiki
	cat document.part1.wiki ho.class.wiki document.part2.wiki info.wiki document.part3.wiki > $@


.PHONY: ho2 $(TARGET_HO2)

ho2: $(TARGET_HO2)

$(TARGET_HO2): $(TARGET_HO2).pdf
	if [ "$(SHOW)" = "yes" ]; then $(VIEWER) $@.pdf ; fi &

$(TARGET_HO2).pdf: $(CMN_DEPS) ho2.pdf
	cp ho2.pdf $@

ho2.pdf: $(CMN_DEPS_TEX) ho2.tex

ho2.tex: $(CMN_DEPS_WIKI) ho2.wiki

ho2.wiki: $(CMN_DEPS_WIKI) ho2.class.wiki
	cat document.part1.wiki ho2.class.wiki document.part2.wiki info.wiki document.part3.wiki > $@


.PHONY: presentation paper handout handout2

presentation: $(TARGET_PRESENTATION)

paper: $(TARGET_PAPER)

handout: $(TARGET_HANDOUT)

handout2: $(TARGET_HANDOUT2)

.PHONY: $(TARGET_PAPER) $(TARGET_PRESENTATION) $(TARGET_HANDOUT) $(TARGET_HANDOUT2)


$(TARGET_PRESENTATION): $(TARGET_PRESENTATION).pdf

$(TARGET_PRESENTATION).pdf: $(IMAGES) presentation.pdf
	cp presentation.pdf $@

presentation.pdf: presentation.tex $(COMMON_DEPS)


$(TARGET_PAPER): $(TARGET_PAPER).pdf

$(TARGET_PAPER).pdf: paper.pdf
	cp paper.pdf $@

paper.pdf: paper.tex $(COMMON_DEPS)


$(TARGET_HANDOUT): $(IMAGES) $(TARGET_HANDOUT).pdf

$(TARGET_HANDOUT).pdf: handout.pdf
	cp handout.pdf $@

handout.pdf: handout.tex $(COMMON_DEPS)


$(TARGET_HANDOUT2): $(TARGET_HANDOUT2).pdf

$(TARGET_HANDOUT2).pdf: $(IMAGES) handout2.pdf
	cp handout2.pdf $@

handout2.pdf: handout2.tex $(COMMON_DEPS)


base.tex: content.tex

content.tex: content.wiki


.wiki.tex:
	wiki2beamer $< > $@

.tex.pdf:
	pdflatex $<
	pdflatex $<
	#if [ "$(SHOW)" = "yes" ]; then $(VIEWER) $@ ; fi &

.dia.png:
	@dia -e $@ $<

.dia.fig:
	@dia -e $@ $<

.dia.svg:
	@dia -e $@ $<


.PHONY: png fig svg images

png: $(DIAIMAGES_PNG)

fig: $(DIAIMAGES_FIG)

svg: $(DIAIMAGES_SVG)

images: $(IMAGES)


.PHONY: archive

archive: $(TARGET).tar.gz

$(TARGET).tar.gz: $(TARGET).tar
	@echo "Creating $@"
	@rm -f $(TARGET).tar.gz
	@gzip $(TARGET).tar


# make $(TARGET).tar phony - it vanishes by gzipping...
.PHONY: $(TARGET).tar

$(TARGET).tar: presentation handout handout2 pr ho ho2
	@echo "Creating $@"
	@git archive --prefix=$(TARGET)/ HEAD > $@
	@rm -rf $(TARGET)
	@mkdir $(TARGET)
	@cp $(TARGET_PRESENTATION).pdf $(TARGET)
	#@cp $(TARGET_PAPER).pdf $(TARGET)
	@cp $(TARGET_HANDOUT).pdf $(TARGET)
	@cp $(TARGET_HANDOUT2).pdf $(TARGET)
	@cp $(TARGET_PR).pdf $(TARGET)
	@cp $(TARGET_HO).pdf $(TARGET)
	@cp $(TARGET_HO2).pdf $(TARGET)
	@tar rf $@ $(TARGET)/$(TARGET_PRESENTATION).pdf
	#@tar rf $@ $(TARGET)/$(TARGET_PAPER).pdf
	@tar rf $@ $(TARGET)/$(TARGET_HANDOUT).pdf
	@tar rf $@ $(TARGET)/$(TARGET_HANDOUT2).pdf
	@tar rf $@ $(TARGET)/$(TARGET_PR).pdf
	@tar rf $@ $(TARGET)/$(TARGET_HO).pdf
	@tar rf $@ $(TARGET)/$(TARGET_HO2).pdf



.PHONY: clean

clean:
	@git clean -f
