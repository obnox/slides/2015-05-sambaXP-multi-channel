title=[MC $\in$ Samba]{SMB3 Multi-Channel in Samba\\ \ }
subtitle={sambaXP 2015}
date={May 20, 2015}
author={Michael Adam}
institute={Samba Team / Red Hat}
